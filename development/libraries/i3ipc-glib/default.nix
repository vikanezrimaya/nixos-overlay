#with import <nixpkgs> {};
{ stdenv, fetchurl, glib, libxcb, xcbproto, json-glib, pkgconfig }:

stdenv.mkDerivation rec {
  name = "i3ipc-glib-${version}";
  version = "0.6.0";
  src = fetchurl {
    url = "https://github.com/acrisci/i3ipc-glib/releases/download/v${version}/i3ipc-${version}.tar.gz";
    sha256 = "b70bde8e307e8e42b01868a070b843fd66e82c3c910e98d187bde109fd420fb8";
  };

  buildInputs = [
    glib libxcb xcbproto json-glib
  ];
  nativeBuildInputs = [ pkgconfig ];

  meta = with stdenv.lib; {
    homepage = https://github.com/acrisci/i3ipc-glib/releases/download/v0.6.0/i3ipc-0.6.0.tar.gz;
    description = "A C interface library to i3wm";
    license = licenses.gpl3;
    platforms = platforms.linux;
  };
}

