{ stdenv, python3Packages }:
python3Packages.buildPythonApplication rec {
  pname = "unrpa";
  version = "2.3.0";

  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "0yl4qdwp3in170ks98qnldqz3r2iyzil5g1775ccg98qkh95s724";
  };

  propagatedBuildInputs = with python3Packages; [
    uncompyle6
  ];

  meta = with stdenv.lib; {
    description = "A program to extract files from the RPA archive format";
    license = with licenses; [ gpl3Plus ];
    maintainers = with maintainers; [ vika_nezrimaya ];
  };
}
