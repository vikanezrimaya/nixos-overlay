final: prev: {
  hashpipe            = final.callPackage tools/security/hashpipe {}; # Not maintained anymore, no updates expected
  i3ipc-glib          = final.callPackage development/libraries/i3ipc-glib {};
  ledgerhelpers       = final.callPackage applications/office/ledgerhelpers {};
  notifymuch          = final.callPackage tools/networking/notifymuch { inherit (final.gnome3) gtk; };
  paswitch            = final.callPackage applications/audio/paswitch {}; # Not maintained anymore, no updates expected
  pidgin-lurch        = final.callPackage applications/networking/instant-messengers/pidgin-plugins/pidgin-lurch {};
  unrpa               = final.callPackage development/tools/unrpa { python3Packages = final.python37Packages; };
  wpgtk               = final.callPackage tools/graphics/wpgtk { inherit (final.gnome3) gtk; };

  # unused
  #python3 = prev.python3.override { packageOverrides = import ./python3.nix final; };
  #python37 = prev.python37.override { packageOverrides = import ./python3.nix final; };
  #python38 = prev.python38.override { packageOverrides = import ./python3.nix final; };

  linuxPackages_4_19 = prev.linuxPackages_4_19 // {
    rtlwifi_new-extended = prev.linuxPackages_4_19.rtlwifi_new.overrideAttrs (old: rec {
      name = "rtlwifi_new-extended-${version}";
      version = "2020-08-19";
      src = prev.fetchFromGitHub {
        owner = "silenoth";
        repo = "rtlwifi_new-extended";
        rev = "cd238b7ab24af635c2e591fd061fd5951ff66249";
        sha256 = "0afd7wnk219cl0c0apm1h6srcbx9ybybsqga3wxxw8zmzn4z0j18";
      };
    });
  };

  vimPlugins = prev.vimPlugins // { # Bugfix for indentations for attrubute sets
    vim-nix = prev.vimUtils.buildVimPluginFrom2Nix {
      name = "vim-nix";
      src = prev.fetchgit {
        url = "https://github.com/LnL7/vim-nix";
        rev = "dae3d30a145f1d0e50658dafd88f95cd4b5e323d";
        sha256 = "1x3gaiz2wbqykzhk0zj0krjp81m5rxhk80pcg96f4gyqp7hxrx78";
      };
      dependencies = [];
    };
  };
  xfce = prev.xfce // {
    xfce4-i3-workspaces-plugin = prev.xfce.callPackage desktops/xfce/panel-plugins/xfce4-i3-workspaces-plugin.nix {
      inherit (final.xfce) gtk;
    };
  };
}
