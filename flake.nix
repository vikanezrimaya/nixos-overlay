{
  description = "Vika's private overlays (that need to be migrated to her private Nixpkgs fork maybe?";

  inputs.nixpkgs = {
    type = "github";
    owner = "NixOS";
    repo = "nixpkgs";
    ref = "nixos-unstable";
  };

  outputs = { self, nixpkgs }: {
    overlay = self.overlays.main;
    overlays = {
      main = import ./default.nix;
      broken = import ./broken.nix;
    };
    checks.x86_64-linux = import ./release.nix {
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ self.overlay ];
      };
    };
  };
}
