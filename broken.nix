# This overlay contains broken, unused and untested packages.
# Enable at your own fucking risk.
# It obviously depends on the main overlay.
final: prev: {
  /*
   * IIRC this compiles, but I don't use it. Did it just for fun and forgot about it.
   */
  linux_libre_4_14 = prev.callPackage os-specific/linux/kernel/linux-libre-4.14.nix {
    kernelPatches = [
      final.kernelPatches.bridge_stp_helper
      final.kernelPatches.cpu-cgroup-v2."4.11"
      final.kernelPatches.modinst_arg_list_too_long
      final.kernelPatches.xen-netfront_fix_mismatched_rtnl_unlock
      final.kernelPatches.xen-netfront_update_features_after_registering_netdev
    ];
  };
  linuxPackages_libre_4_14 = prev.linuxPackagesFor final.linux_libre_4_14;

  /*
   * Completely no way to make this stuff work.
   * I really don't know how it works.
   */
  ludget = prev.callPackage applications/office/ludget {
    matplotlib = prev.python3Packages.matplotlib.override { enableGtk3 = true; };
  };

  /*
   * This thing doesn't build because new Qt build system is shit that 
   * Nix doesn't wrap yet. I don't wanna fuck with it.
   */
  mellowplayer = prev.libsForQt5.callPackage applications/audio/mellowplayer {};

  /* 
   * TODO: refactor ruby bindings into a separate package... somehow
   * All Ruby packages are USUALLY packaged via gems, but notmuch doesn't seem
   * to have one for obvious reasons (it has a binary - how do binaries in gems
   * even work?
   */
  notmuch = prev.notmuch.overrideAttrs (oldAttrs: rec {
    # Actually making ruby dependency useful there
    buildFlags = [ "ruby-bindings" ];
    postInstall = ''
      make install-man
      cd bindings/ruby && make install prefix=$out
    '';
  });

  /*
   * This can be potentially used to make Ren'Py support webp format
   * Untested, but seems to compile
   * Ren'Py support on NixOS is flaky anyway
   */
  SDL2_image = prev.callPackage development/libraries/SDL2_image {
    inherit (final.darwin.apple_sdk.frameworks) Foundation;
  };

  /*
   * Haven't been updated and broke. Still nice to have a Nix expression for it.
   */
  lightlockersettings = prev.callPackage tools/X11/light-locker-settings { inherit (final.gnome3) gtk; };

  xfce = prev.xfce // {
    /*
     * Can't find libindicator for some reason.
     */
    xfce4-indicator-plugin = prev.xfce.callPackage desktops/xfce/panel-plugins/xfce4-indicator-plugin.nix {
      inherit (final.xfce) gtk;
      libappindicator = final.libappindicator-gtk2;
      libindicator = final.libindicator-gtk2;
    };
  };

  /*
   * I don't even remember why.
   */
  lua5 = prev.lua5.override {
    packageOverrides = lfinal: lprev: {
      luapam = lfinal.callPackage ({ stdenv, buildLuaPackage, fetchFromGitHub }: buildLuaPackage rec {
        name = "lua-pam";
        src = fetchFromGitHub {
          owner = "devurandom";
          inherit name;
          rev = "3818ee6346a976669d74a5cbc2a83ad2585c5953";
          sha256 = "0000000000000000000000000000000000000000000000000000";
        };
      }) {};
    };
  };
  
}
