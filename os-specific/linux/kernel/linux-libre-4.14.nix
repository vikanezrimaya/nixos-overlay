{ stdenv, buildPackages, hostPlatform, fetchurl, perl, buildLinux, modDirVersionArg ? null, ... } @ args:

with stdenv.lib;

buildLinux (args // rec {
  version = "4.14.59-gnu";

  # modDirVersion needs to be x.y.z, will automatically add .0 if needed
  modDirVersion = if (modDirVersionArg == null) then concatStrings (intersperse "." (take 3 (splitString "." "${version}.0"))) else modDirVersionArg;

  # branchVersion needs to be x.y
  extraMeta.branch = concatStrings (intersperse "." (take 2 (splitString "." version)));

  src = fetchurl {
    #url = "mirror://kernel/linux/kernel/v4.x/linux-${version}.tar.xz";
    #sha256 = "16ribg80jk830wyk4k7v86jysmnkj59v62rlkqil3advc7337iky";
    url = "http://linux-libre.fsfla.org/pub/linux-libre/releases/${version}/linux-libre-${version}.tar.xz";
    sha256 = "1mf22i8a71qs04x4wfqmm21clj4jnqia6rpk7jbh3r3vjfjjbd1d";
  };
} // (args.argsOverride or {}))
