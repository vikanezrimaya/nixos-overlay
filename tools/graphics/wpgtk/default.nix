{ stdenv, fetchFromGitHub, python3Packages, wrapGAppsHook, gobjectIntrospection, libxslt, gtk, pywal }:

python3Packages.buildPythonApplication rec {
  pname = "wpgtk";
  version = "6.0.1";

  src = fetchFromGitHub {
    owner = "deviantfero";
    repo = pname;
    rev = "${version}";
    sha256 = "1g85lpp3zscdvr8akrzwkpsd5y8n2b10rwx0prw40b4fkqjvr8vy";
  };

  preBuild = ''
    mkdir tmp
    HOME=$PWD/tmp
  '';

  nativeBuildInputs = [ wrapGAppsHook ];

  buildInputs = [
    gobjectIntrospection libxslt gtk
  ];

  propagatedBuildInputs = with python3Packages; [
    pygobject3 pillow pywal 
  ];

  # There are no tests
  doCheck = false;

  meta = with stdenv.lib; {
    homepage = https://deviantfero.github.io/wpgtk;
    description = "A colorscheme, wallpaper and template manager for *nix";
    license = licenses.gpl2;
  };
}
