{ stdenv, fetchFromGitHub, python3Packages, wrapGAppsHook,
  lightlocker, gobjectIntrospection, gtk, intltool, which }:
python3Packages.buildPythonApplication rec {
  pname = "light-locker-settings";
  version = "1.5.3";

  src = fetchFromGitHub {
    owner = "Antergos";
    repo = pname;
    rev = "${version}";
    sha256 = "10rs0lmhf6za44lclj43i95zbmh4aq0axya38h6gdzbvvv6zih5p";
  };

  nativeBuildInputs = [ wrapGAppsHook intltool which ];
  buildInputs = [ gobjectIntrospection gtk ];
  propagatedBuildInputs = with python3Packages; [ psutil pygobject3 ];

  configurePhase = ''
    ./configure --python=${python3Packages.python.withPackages (ps: propagatedBuildInputs)}/bin/python --prefix=$out
  '';
  buildPhase = ''
    make
  '';
  installPhase = ''
    make install
  '';

  doCheck = false;
  meta = with stdenv.lib; {
    description = "A simple GUI configuration utility for light-locker";
    license = licenses.gpl3;
  };
}
