{ stdenv, fetchFromGitHub, python3Packages, notmuch, wrapGAppsHook, gobjectIntrospection, gtk, libnotify }:

python3Packages.buildPythonApplication rec {
  pname = "notifymuch";
  version = "0.1";
  src = fetchFromGitHub {
    owner = "kspi";
    repo = pname;
    rev = "9d4aaf54599282ce80643b38195ff501120807f0";
    sha256 = "1lssr7iv43mp5v6nzrfbqlfzx8jcc7m636wlfyhhnd8ydd39n6k4";
  };

  nativeBuildInputs = [ wrapGAppsHook ];

  buildInputs = [
    gobjectIntrospection gtk notmuch libnotify
  ];

  propagatedBuildInputs = with python3Packages; [
    pygobject3 python3Packages.notmuch
  ];

  # There are no tests
  doCheck = false;

  meta = with stdenv.lib; {
    homepage = https://github.com/kspi/notifymuch;
    description = "Display desktop notifications for unread mail in notmuch database";
    license = licenses.gpl3;
    hydraPlatforms = [];
  };
}
