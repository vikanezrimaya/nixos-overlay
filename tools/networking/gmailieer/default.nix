{ stdenv, fetchFromGitHub, python3Packages, notmuch }:

python3Packages.buildPythonApplication rec {
  pname = "gmailieer";
  version = "0.10";

  src = fetchFromGitHub {
    owner = "gauteh";
    repo = pname;
    rev = "v" + version;
    sha256 = "0qv74marzdv99xc2jbzzcwx3b2hm6byjl734h9x42g4mcg5pq9yf";
  };
  propagatedBuildInputs = with python3Packages; [
    python3Packages.notmuch google_api_python_client oauth2client tqdm
  ];

  makeWrapperArgs = [
    ''--prefix PATH ':' "${notmuch}/bin"''
  ];

}
