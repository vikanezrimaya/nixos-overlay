{ stdenv, fetchurl, openssl }:

stdenv.mkDerivation rec {
  name = "hashpipe";
  rev = "dc11b6262f4717e61e55760cb2bd637ff1c0f6a9";


  src = fetchurl {
    url = "https://git.zx2c4.com/hashpipe/snapshot/${name}-${rev}.tar.xz";
    sha256 = "1xhlqv9pfg0jf6jfilpdi1wvlra0ja8lzy87dhmbwck3qkbk2fbb";
  };

  buildInputs = [ openssl ];

  installFlags = [ "PREFIX=$(out)" ];
 
  meta = {
    description = "Verify hashes in pipeline";
    license = stdenv.lib.licenses.gpl2;
    platforms = stdenv.lib.platforms.all;
  };
}
