{ stdenv, fetchurl, pkgconfig, intltool, libxfce4util, xfce4-panel, libxfce4ui, xfconf, gtk, hicolor-icon-theme, libappindicator, libindicator }:

stdenv.mkDerivation rec {
  p_name  = "xfce4-indicator-plugin";
  ver_maj = "0.3";
  ver_min = "0.0";
  
  src = fetchurl {
    url = "mirror://xfce/src/panel-plugins/${p_name}/${ver_maj}/${name}.tar.bz2";
    sha256 = "19lamx6imy980kjma3x2ycpzdxz413blymxjkgb0qnkf1pwl4j8q";
  };
  name = "${p_name}-${ver_maj}.${ver_min}";

  nativeBuildInputs = [ pkgconfig ];
  buildInputs = [ intltool libxfce4util libxfce4ui xfce4-panel xfconf gtk hicolor-icon-theme libappindicator libindicator ];
  
  meta = {
    homepage = "http://goodies.xfce.org/projects/panel-plugins/${p_name}";
    description = "A small plugin to display information from various applications consistently in the panel as described in Ubuntus MessagingMenu design specification";
    platforms = stdenv.lib.platforms.linux;
    broken = true; # Can't find libindicator?
  };
}
