#with import <nixpkgs> {};
{ stdenv, fetchurl, glib, gtk, libxfce4ui, libxfce4util, xfce4-panel, i3ipc-glib, xfce4-dev-tools, pkgconfig }:

stdenv.mkDerivation rec {
  name = "xfce4-i3-workspaces-plugin-${version}";
  version = "1.2.0";
  src = fetchurl {
    url = "https://github.com/denesb/xfce4-i3-workspaces-plugin/releases/download/${version}/xfce4-i3-workspaces-plugin-${version}.tar.gz";
    sha256 = "4e2daa9304031043b769f7db0f6ddd2140efeb0e1c52c47be5c3e3f1666b7086";
  };

  buildInputs = [
    glib
    gtk
    libxfce4ui libxfce4util xfce4-panel
    i3ipc-glib
  ];

  nativeBuildInputs = [
    pkgconfig
    xfce4-dev-tools
  ];
  
  meta = with stdenv.lib; {
    homepage = https://github.com/denesb/xfce4-i3-workspaces-plugin;
    description = "A workspaces plugin for xfce4 and the i3 window manager";
    license = licenses.gpl3;
    platforms = platforms.linux;
  };
}
