#!/usr/bin/env bash
{
    echo "\
let
  pkgs = import <nixpkgs> { overlays = [ (import ./default.nix) ]; };
in {"
#grep -e callPackage -e linuxPackagesFor default.nix | sed -e 's/^ *\([^ ]*\) * = * super\(.*\)(callPackage|linuxPackagesFor) .*/  \1 = pkgs\2\1;/g'
grep -ve "^ *#" default.nix | grep -e linuxPackagesFor -e callPackage | sed \
    -e 's/^ *\([^ ]*\) * = * super\(.*\)\.callPackage .*/  \1 = pkgs\2.\1;/g' \
    -e 's/^ *\([^ ]*\) * = * super\(.*\)\.linuxPackagesFor .*/  inherit (pkgs\2) \1;/g' \
    -e 's/\.libsForQt5//g'
    echo "}"
} 
