{ pkgs ? (import <nixpkgs> { overlays = [ (import ./default.nix) ]; }) }:
{
  hashpipe = pkgs.hashpipe;
  i3ipc-glib = pkgs.i3ipc-glib;
  ledgerhelpers = pkgs.ledgerhelpers;
  notifymuch = pkgs.notifymuch;
  paswitch = pkgs.paswitch;
  pidgin-lurch = pkgs.pidgin-lurch;
  unrpa = pkgs.unrpa;
  wpgtk = pkgs.wpgtk;
  xfce4-i3-workspaces-plugin = pkgs.xfce.xfce4-i3-workspaces-plugin;

  rtlwifi_new-extended = pkgs.linuxPackages_4_19.rtlwifi_new-extended;
}
