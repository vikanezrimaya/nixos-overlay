{ lib, mkDerivation, fetchFromGitHub, 
  qmake, qbs, pkgconfig, which,
  libnotify,
  qtbase, qtwebengine, qtquickcontrols2
}:

mkDerivation rec {
  name = "MellowPlayer";
  version = "3.4.0";

  src = fetchFromGitHub {
    owner  = "ColinDuquesnoy";
    repo   = "MellowPlayer";
    rev    = version;
    sha256 = "0v74zq7r3nw1ay4pgr2czssd95s6mvyd89g96ixr86a70ks9g0pa";
  };

  buildInputs = [ qtbase qtwebengine qtquickcontrols2 libnotify ];
  nativeBuildInputs = [ qmake qbs pkgconfig which ];

  configurePhase = ''
    qmake -query
    qbs-setup-toolchains --detect
    qbs-setup-qt $(which qmake) qt5
    qbs-config defaultProfile qt5
  '';

  buildPhase = ''
    qbs build config:release
  '';

  installPhase = ''
    qbs install --install-root $out/ config:release
  '';

  meta = with lib; {
    description = "Cloud music integration for your desktop";
    license = llicenses.gpl3;
  };
}
