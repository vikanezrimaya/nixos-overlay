{ stdenv, fetchgit, libpulseaudio }:
stdenv.mkDerivation rec {
  name = "paswitch";
  src = fetchgit {
    url = "https://www.tablix.org/~avian/git/paswitch.git";
    rev = "1b900dae95068be5f72cf679c889c0c12b01091b";
    sha256 = "0403rl2wpyymbxzvqj2r00sb3q4j63rhchsa1x6dkyvmdkg1xahr";
  };
  buildInputs = [ libpulseaudio ];
  installPhase = ''
    install -Dm 755 paswitch $out/bin/paswitch
  '';
  meta = with stdenv.lib; {
    description = "PulseAudio sink switcher with logic extracted from gnome-volume-control";
  };
}

