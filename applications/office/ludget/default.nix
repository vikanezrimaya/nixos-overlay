{ stdenv, python3, python3Packages, ledger, matplotlib }:

python3Packages.buildPythonApplication rec {
  pname = "ludget";
  version = "0.1.0";
  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "0ii88pxqq72lmhiz9pprnfbv44b9y5mvg09841bvxyhz4jz38kb9";
  };
  propagatedBuildInputs = [
    ledger
    # Custom matplotlib here
    matplotlib
  ] ++ (with python3Packages; [
    numpy
    pandas
  ]);
  meta = {
    homepage = http://gitlab.com/rjurga/ludget;
    description = "ledger-cli data visualisation";
    license = stdenv.lib.licenses.gpl3;
  };
}
