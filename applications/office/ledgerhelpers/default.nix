{ stdenv, fetchFromGitHub, wrapGAppsHook, python2Packages, lib, ledger, gtk3, gobjectIntrospection }:

python2Packages.buildPythonApplication rec {
  pname = "ledgerhelpers";
  version = "0.2.3";

  src = fetchFromGitHub {
    owner = "Rudd-O";
    repo = pname;
    rev = "v${version}";
    sha256 = "0y3a0sk3a3csqzpwm29na0sq209bxa95qm6l1pzbqypi93nwj4sa";
  };

  nativeBuildInputs = [ wrapGAppsHook ];
  buildInputs = [ gtk3 gobjectIntrospection ];
  propagatedBuildInputs = [ ledger python2Packages.pygobject3 ];

}
