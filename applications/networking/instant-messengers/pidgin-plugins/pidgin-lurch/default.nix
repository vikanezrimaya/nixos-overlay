#with import <nixpkgs> {};
{ stdenv, fetchurl, cmake, pidgin, minixml, libxml2, sqlite, libgcrypt }:

stdenv.mkDerivation rec {
  name = "pidgin-lurch-${version}";
  version = "0.6.8";
  src = fetchurl {
    url = "https://github.com/gkdr/lurch/releases/download/v${version}/lurch-${version}-src.tar.gz";
    sha256 = "17xis9dp0ncgxbsqncvfbml900n78sla4yccipqf86kbzsslf91f";
  };
  #sourceRoot = ".";

  installPhase = ''
    mkdir -p $out/lib/pidgin
    mv build/lurch.so $out/lib/pidgin
  '';
  dontUseCmakeConfigure = true;


  nativeBuildInputs = [ cmake ];
  buildInputs = [ pidgin minixml libxml2 sqlite libgcrypt ];

  meta = with stdenv.lib; {
    homepage = https://github.com/gkdr/lurch;
    description = "OMEMO for libpurple - interoperable with other OMEMO clients";
    license = licenses.gpl3;
    platforms = platforms.linux;
  };
}
